﻿# Vkimd

**VKIMD** is an application, which logs your private messages of VK social media to a file & stdout.

## Usage

1. Download the project from CI (only Windows and Linux 64x runtimes are prebuilt).
2. Launch the application. The default working directory is the user directory. You can specify it as a command-line option: `Vkimd ~/MyDirectory`
3. The application will create a `vkimd` subdirectory there and place its config file: `config.json`.
4. In the config you should place your VK password and login.
5. Restart the application. The logs will be in `vkimd/logs` folder.

## Example `config.json`

```json
{
  "Credentials": {
    "Password": "qwerty",
    "Login": "user@example.com"
  }
}

```

## Building the project manually

You can build the project for your runtime: `dotnet publish -r <runtime identifier> -c Release /p:PublishSingleFile=true`. Find your Runtime Identifier in the [Microsoft catalog](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog). 

## Licensing

This project is licensed under the [MIT license](LICENSE).
