﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Vkimd.Configuration
{
    [Serializable]
    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public sealed class Credentials : IEquatable<Credentials>
    {
        [JetBrains.Annotations.NotNull] public string Password { get; set; } = "";

        public string Login { get; set; } = "";

        [Pure]
        public bool Equals(Credentials other)
        {
            if (ReferenceEquals(null, other))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return string.Equals(Password, other.Password, StringComparison.CurrentCulture) && Login == other.Login;
        }

        [Pure]
        public override bool Equals(object obj) =>
            ReferenceEquals(this, obj) || obj is Credentials other && Equals(other);

        [Pure]
        public override string ToString() => $"{nameof(Password)}: {Password}, {nameof(Login)}: {Login}";

        [Pure]
        public override int GetHashCode()
        {
            unchecked
            {
                return (StringComparer.CurrentCulture.GetHashCode(Password) * 397) ^ Login.GetHashCode();
            }
        }

        [Pure]
        public static bool operator ==([CanBeNull] Credentials left, [CanBeNull] Credentials right) =>
            Equals(left, right);

        [Pure]
        public static bool operator !=([CanBeNull] Credentials left, [CanBeNull] Credentials right) =>
            !Equals(left, right);
    }
}