﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.IO;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Vkimd.Configuration
{
    [Serializable]
    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
    public sealed class VkimdConfiguration : IEquatable<VkimdConfiguration>
    {
        [JetBrains.Annotations.NotNull]
        public static VkimdConfiguration Instance { get; private set; } = new VkimdConfiguration();

        [JetBrains.Annotations.NotNull] private static readonly string ConfigurationFile =
            Path.Combine(Vkimd.WorkingDirectory, "config.json");

        [JetBrains.Annotations.NotNull]
        private static readonly ILogger Logger = Vkimd.LoggerFactory.CreateLogger(typeof(VkimdConfiguration));

        public static void Initialize()
        {
            try
            {
                Read();
            }
            catch (IOException)
            {
                Logger.LogInformation("It is invalid configuration in {0}, writing the default one.",
                    ConfigurationFile);

                Write();
            }
        }

        private static void Read()
        {
            Logger.LogDebug("Reading configuration from {0}", ConfigurationFile);
            var json = File.ReadAllText(ConfigurationFile);
            Logger.LogTrace("json = {0}", json);
            Instance = JsonConvert.DeserializeObject<VkimdConfiguration>(json);
            Logger.LogDebug("Read configuration: {0}.", Instance);
        }

        private static void Write()
        {
            Logger.LogDebug("Writing configuration: {0}.", Instance);
            var json = $"{JsonConvert.SerializeObject(Instance, Formatting.Indented)}\n";
            Logger.LogTrace("json = {0}", json);
            File.WriteAllText(ConfigurationFile, json);
        }

        [JetBrains.Annotations.NotNull] public Credentials Credentials { get; set; } = new Credentials();

        [JetBrains.Annotations.Pure]
        public bool Equals(VkimdConfiguration other)
        {
            if (ReferenceEquals(null, other))
                return false;

            return ReferenceEquals(this, other) || Equals(Credentials, other.Credentials);
        }

        [JetBrains.Annotations.Pure]
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            return obj.GetType() == GetType() && Equals((VkimdConfiguration) obj);
        }

        [JetBrains.Annotations.Pure]
        public override int GetHashCode() => Credentials.GetHashCode();

        [JetBrains.Annotations.Pure]
        public static bool operator ==([CanBeNull] VkimdConfiguration left, [CanBeNull] VkimdConfiguration right) =>
            Equals(left, right);

        [JetBrains.Annotations.Pure]
        public static bool operator !=([CanBeNull] VkimdConfiguration left, [CanBeNull] VkimdConfiguration right) =>
            !Equals(left, right);

        [JetBrains.Annotations.Pure]
        public override string ToString() => $"{nameof(Credentials)}: {Credentials}";
    }
}