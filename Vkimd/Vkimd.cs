﻿using System;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Vkimd.Configuration;
using VkNet;
using VkNet.AudioBypassService.Extensions;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Vkimd
{
    internal static class Vkimd
    {
        private const string AppName = "vkimd";
        [NotNull] public static ILoggerFactory LoggerFactory { get; }
        [NotNull] private static readonly ILogger Logger;

        static Vkimd()
        {
            LoggerFactory = Microsoft.Extensions.Logging.LoggerFactory
                .Create(builder => { builder.AddConsole().AddDebug().SetMinimumLevel(LogLevel.Information); });

            Logger = LoggerFactory.CreateLogger(typeof(Vkimd));
            var serviceCollection = new ServiceCollection();
            serviceCollection.TryAddSingleton(LoggerFactory.CreateLogger<VkApi>());
            serviceCollection.AddAudioBypass();
            Vk = new VkApi(serviceCollection);
        }

        [NotNull] public static VkApi Vk { get; }

        [NotNull]
        public static string WorkingDirectory { get; private set; } =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), AppName);

        private static void Main(string[] args)
        {
            if (args.Length != 0)
                WorkingDirectory = args.First();
            else
                Logger.LogInformation(
                    "Setting default working directory. It also can be specified as a command-line argument");

            LoggerFactory.AddFile(Path.Combine(WorkingDirectory, "logs", $"{AppName}-{{Date}}.txt"));
            Logger.LogInformation("The working directory is {0}", WorkingDirectory);
            Directory.CreateDirectory(WorkingDirectory);
            VkimdConfiguration.Initialize();
            MessageAcceptor.Initialize();
        }
    }
}