﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Vkimd.Configuration;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;

namespace Vkimd
{
    public static class MessageAcceptor
    {
        private const int LpVersion = 3;
        private static ulong _ts;
        private static ulong _pts;

        [NotNull] private static readonly ILogger Logger = Vkimd.LoggerFactory.CreateLogger(typeof(MessageAcceptor));

        public static void Initialize()
        {
            Logger.LogInformation("Initializing message acceptor.");

            if (TryAuthorize())
                return;

            Logger.LogInformation("Authorized successfully.");
            var response = Vkimd.Vk.Messages.GetLongPollServer(true, LpVersion);

            if (response.Pts != null)
                _pts = response.Pts.Value;
            else
            {
                Logger.LogError("Cannot connect to long-poll server.");
                return;
            }

            _ts = ulong.Parse(response.Ts);
            StartLongPollLoop();
        }

        private static void StartLongPollLoop()
        {
            while (true)
            {
                var poll = Vkimd.Vk.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
                {
                    Pts = _pts,
                    Ts = _ts,
                    LpVersion = LpVersion
                });

                if (poll?.Messages == null)
                    continue;

                _pts = poll.NewPts;

                poll.Messages.ToList().ForEach(message =>
                {
                    if (message.Deleted == true)
                        return;

                    Logger.LogInformation(message.PrettyPrint());
                });
            }
        }

        private static bool TryAuthorize()
        {
            try
            {
                Vkimd.Vk.Authorize(new ApiAuthParams
                {
                    Password = VkimdConfiguration.Instance.Credentials.Password,
                    Login = VkimdConfiguration.Instance.Credentials.Login,
                    ApplicationId = 123456,
                    Settings = Settings.All
                });
            }
            catch (Exception exception)
            {
                Logger.LogError(exception, "Cannot authorize the user: \"{0}\".", exception.Message);
                return true;
            }

            return false;
        }

        // ReSharper disable once InconsistentNaming
        private static string joinUsersWithID(long id) =>
            string.Join(',', Vkimd.Vk.Users
                .Get(new List<long> {id})
                .Select(user => $"{user.FirstName} {user.LastName}"));

        [CanBeNull]
        private static string PrettyPrint([NotNull] this Message message, ulong forwardedLevel = 0L)
        {
            if (message.FromId == null)
            {
                Logger.LogDebug(
                    "VkApi has provided message with null FromId: {0}. It can be a bug.",
                    message.ToString());

                return null;
            }

            var info = new StringBuilder().AppendFormat(
                "Message from {0}. ",
                joinUsersWithID(message.FromId.Value));

            if (message.PeerId != null)
                info.AppendFormat("Conversation with: {0}. ", joinUsersWithID(message.PeerId.Value));

            if (!string.IsNullOrWhiteSpace(message.Text))
                info.AppendFormat("Text: \"{0}\". ", message.Text);
            
            AppendPhotos(message, info);

            if (message.ForwardedMessages.Count > 0)
                message.ForwardedMessages
                    .ToList()
                    .ForEach(forwarded =>
                    {
                        AppendForwardLevelArrows(forwardedLevel, info);
                        info.AppendFormat("Forwarded message: {0}", forwarded.PrettyPrint(forwardedLevel + 1));
                    });

            if (message.ReplyMessage == null)
                return info.ToString();

            AppendForwardLevelArrows(forwardedLevel, info);
            info.AppendFormat("Reply message: {0}", message.ReplyMessage.PrettyPrint(forwardedLevel + 1));
            return info.ToString();
        }

        private static void AppendPhotos(Message message, StringBuilder info)
        {
// ReSharper disable once SuspiciousTypeConversion.Global
            var photos = message.Attachments
                .Where(attachment => attachment.Instance is Photo)
                .Select(attachment => attachment.Instance)
                .Cast<Photo>()
                .ToList();

            if (photos.Count <= 0)
                return;
            
            info.Append("Photos: ");

            info.AppendJoin(", ",
                photos.Select(photo => photo.Sizes.Aggregate((PhotoSize) null, (accumulate, size) =>
                {    
                    if (accumulate == null || size.Height > accumulate.Height)
                        return size;

                    return accumulate;
                }).Url.ToString()));

            info.Append(". ");
        }

        private static void AppendForwardLevelArrows(ulong forwardedLevel, StringBuilder info)
        {
            info.Append("\n");

            for (ulong i = 0L; i < forwardedLevel; i++)
                info.Append("|>");
        }
    }
}